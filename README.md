# DesireMachine

Here's a place to jot down ideas related to *Desire machine* (aka. *machina eterna*).

## Concept

Putting movement in different sapces. Eg. body movement - light. 

- Body to light **→ specifically: bounded vs. infinite movement regimes**

- *Nachklang* of body movement turned into light.

- hypnosis, like a pendulum

### Constraints

* 1 dancer

* DMX lights (white, any amount)

* All movement originates -or ends- in the body

---

### Development

- [Trello board of the project!](https://trello.com/b/WzWzC5eg/desirelab)
- [Gantt](https://app.teamgantt.com/projects/gantt?ids=3073574) graph, [exported](https://prod.teamgantt.com/gantt/export/pdf/?baselines=&color=default&color_filter=&company_resources=&date_filter=&date_format=&display_dependencies=1&display_name_in_bars=0&display_resources=1&hide_completed=0&orientation=p&page_size=Letter&pdf_font_face=dejavusans&pdf_font_size=7&pdf_task_date_format=&project_resources=&projects=3073574&public_keys=&rand=193941&show_estimated_hours_column=1&show_name_next_to_bar=0&show_percent_column=1&show_project_name_on_bar=0&task_list=default&user_date=2022-09-18&user_resources=) and [added](https://docs.google.com/drawings/d/1YSN-AOCBenrTd1i1ysxx705AQE9i5guIJrYjz0sJoRA/edit) to [GDrive](https://drive.google.com/drive/folders/1cvpwEl5vzOtddfsV3cPgmIWFjM9PkF-X).

![Gantt screen capture](https://docs.google.com/drawings/d/e/2PACX-1vSH2ic657ZRkOTyFr51MG7TeVK9Qi2GlrdBl7vJiIsNB0yNCQbVSWDTd2_nGYRoqGTsk6V_rGmBsYpe/pub?w=931&h=490)

### Dependencies

- [~~DMX4Live - M4L device~~](https://bitbucket.org/AdrianArtacho/dmx4live)

- [DesireMachine - M4L device](https://bitbucket.org/smoothspaces/desiremachine)

- [OSC-StartStop - M4L device](https://bitbucket.org/smoothspaces/osc-startstop)

- [mp_hands_rt - python implementation](https://bitbucket.org/smoothspaces/mp_hands_rt) based on [MediaPipe](https://mediapipe.dev/)

- [Custom smartphone App](https://bitbucket.org/AdrianArtacho/synclab-droid/)

---

### Implementation

The technical implementation of the piece is described in the [diagram](https://docs.google.com/drawings/d/1Zo-6Jwjdn_C0dkY1AgF9dkEv1ujN5RqCwg64nLeUe9E/edit) below:

![RealTime - elements of the setup](https://docs.google.com/drawings/d/e/2PACX-1vSE7e8FWFgE8QVe4D9MQqPhPeYaeZvE1VIkwXcwmL7esgPPYUBLvv-oEbjtscT76Clk8lG9XDGkqoDK/pub?w=1019&h=673)

### Incoming movement data

A [python script](https://bitbucket.org/smoothspaces/mp_hands_rt) processes the incoming video images from a webcam using an implementation of [MediaPipe](https://mediapipe.dev/) and extracts the *pose landmarks* accordimng to this [landmark definition]([Pose - mediapipe</title> <link rel="shortcut icon" href="/mediapipe/favicon.ico" type="image/x-icon"> <link rel="stylesheet" href="/mediapipe/assets/css/just-the-docs-default.css"> <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140696581-2"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-140696581-2'); </script> <script type="text/javascript" src="/mediapipe/assets/js/vendor/lunr.min.js"></script> <script type="text/javascript" src="/mediapipe/assets/js/just-the-docs.js"></script> <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Begin Jekyll SEO tag v2.8.0 --> <title>Pose | mediapipe</title> <meta name="generator" content="Jekyll v3.9.2" /> <meta property="og:title" content="Pose" /> <meta property="og:locale" content="en_US" /> <meta name="description" content="Cross-platform, customizable ML solutions for live and streaming media." /> <meta property="og:description" content="Cross-platform, customizable ML solutions for live and streaming media." /> <link rel="canonical" href="https://google.github.io/mediapipe/solutions/pose.html" /> <meta property="og:url" content="https://google.github.io/mediapipe/solutions/pose.html" /> <meta property="og:site_name" content="mediapipe" /> <meta property="og:type" content="website" /> <meta name="twitter:card" content="summary" /> <meta property="twitter:title" content="Pose" /> <script type="application/ld+json"> {"@context":"https://schema.org","@type":"WebPage","description":"Cross-platform, customizable ML solutions for live and streaming media.","headline":"Pose","publisher":{"@type":"Organization","logo":{"@type":"ImageObject","url":"https://google.github.io/mediapipe/images/logo_horizontal_color.png"}},"url":"https://google.github.io/mediapipe/solutions/pose.html"}</script> <!-- End Jekyll SEO tag --> </head> <body> <svg xmlns="http://www.w3.org/2000/svg" style="display: none;"> <symbol id="svg-link" viewBox="0 0 24 24"> <title>Link</title> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-link"> <path d="M10 13a5 5 0 0 0 7.54.54l3-3a5 5 0 0 0-7.07-7.07l-1.72 1.71"></path><path d="M14 11a5 5 0 0 0-7.54-.54l-3 3a5 5 0 0 0 7.07 7.07l1.71-1.71"></path> </svg> </symbol> <symbol id="svg-search" viewBox="0 0 24 24"> <title>Search</title> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"> <circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line> </svg> </symbol> <symbol id="svg-menu" viewBox="0 0 24 24"> <title>Menu</title> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"> <line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line> </svg> </symbol> <symbol id="svg-arrow-right" viewBox="0 0 24 24"> <title>Expand</title> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"> <polyline points="9 18 15 12 9 6"></polyline> </svg> </symbol> <symbol id="svg-doc" viewBox="0 0 24 24"> <title>Document</title> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"> <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline> </svg> </symbol> <!-- Feather. MIT License: https://github.com/feathericons/feather/blob/master/LICENSE --> <symbol id="svg-external-link" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-external-link"> <title id="svg-external-link-title">(external link)](https://google.github.io/mediapipe/solutions/pose.html#pose-landmark-model-blazepose-ghum-3d)):

![full_body_landmarks](https://mediapipe.dev/images/mobile/pose_tracking_full_body_landmarks.png)

Additionally, an [OSC server]([python-osc · PyPI](https://pypi.org/project/python-osc/)) listens to port ```udp 53535``` for the IMU movement data coming from a [custom smartphone app](https://bitbucket.org/AdrianArtacho/synclab-droid/).

### Processing movement data

The movement data is processed...

Output of this data processing is sent via OSC and looks like this:

```
/dm/energy [float between 0. and 1.]
/dm/newsegment [float between 0. and 1.]
```

#### LightWall simulation

![LightWall](https://bitbucket.org/artachoscores/desiremachine/raw/08bea6aeec5e6609ad90c747243e70efa75dcc69/images/optimized_wall.gif)

### Interaction design

Based on the movement data input...

---

### Music

- work with delays?

- how to sync sound with the **up gesture**? probably *cued with* **SyncLab**.

- Idea of the regular 'booming hit' sounds, which set a tempo, and the light that lights on with the onset, and disappear slowly with the *nachklang* of the hits.

### Ideas

- Have the sound mediate the light info: movement poduces a sound, the intensity of the sound controls the decay of the lamp light

- Working with more DMX lamps, have the fixture be chosen at random...

- Super sensitive section, where very little movement is VERY reactive (flickering), but still reactive to large movements

- Different lamps with different ramp times

- control different lamps based on planes of the kinesphere

- Have a device to define parameter ranges during rehearsal

- If both phones move together (at the same pace) the resulting light intensity is stronger

- To prime the dancer with a specific sound: then Maria can execute a sudden movement that cuts down light (and sound)

- Opposite arm and leg

- Differential of height between the both?

- reverse the ramp idea?

- The flickering between many lamps

- The original dynamo effect (ramp down after some movement) routed to a random fixture each time, with a intensity-dependent duration... (one Tesser_ramp per fixture + Tesser_random)

- Equating image and sign, which are both singularities in the field of actualisation.

- Use also the flashlight controlled by the movement

### References

[Les Grands Fantômes](https://www.medici.tv/ballets/yoann-bourgeois-monuments-en-mouvement-les-grands-fantomes-pantheon-2017)) by [Yoann Bourgeois](https://en.wikipedia.org/wiki/Yoann_Bourgeois).

[*Claire de lune*](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiB4M72nK_3AhUEiv0HHennDycQwqsBegQIAhAB&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DSFRiHQ-Lwzk&usg=AOvVaw39VPUcTH1seb82SAVNVZrt) by *Yoann Bourgeois*.

[La méchanique de l'histoire](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwi_m6ODna_3AhXjhP0HHVECDiIQwqsBegQIBRAB&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DvNWWSf_NR4E&usg=AOvVaw2Zj98P-YCh60EiWalFKKLi) (Bourgeois)

Flux light bars.

Das LOT: [MOSAÏQUE02 - Open Call für alle Disziplinen](https://www.lot.wien/mosaique) bis **16.5**

[Schmiede Playfround of ideas](https://schmiedehallein.com/)

[Olafur Eliasson](https://olafureliasson.net/archive/artwork/WEK101824/beauty#slideshow)

[Olafur Eliasson, Beauty](https://olafureliasson.net/archive/artwork/WEK101824/beauty#slideshow)

---

# To-Do

* Refresh the M4L devices: I don't want them to be stored in the project folder, but to load from **User Library** (hence actualising) very time.

* Record session's movement data
